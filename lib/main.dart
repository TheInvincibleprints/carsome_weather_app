import 'package:carsomeweatherapp/src/enums/app_state.dart';
import 'package:carsomeweatherapp/src/screens/main_screen.dart';
import 'package:carsomeweatherapp/src/screens/routes.dart';
import 'package:flutter/material.dart';

class WeatherApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: AppStateContainer.of(context).theme,
      home: MainScreen(),
      routes: Routes.mainRoute,
    );
  }
}

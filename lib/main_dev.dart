import 'package:bloc/bloc.dart';
import 'package:carsomeweatherapp/main.dart';
import 'package:carsomeweatherapp/src/bloc_delegate.dart';
import 'package:carsomeweatherapp/src/config/flavor_config.dart';
import 'package:carsomeweatherapp/src/constants/app_constants.dart';
import 'package:carsomeweatherapp/src/enums/app_state.dart';
import 'package:flutter/material.dart';

void main() {
  FlavorConfig(
      flavor: Flavor.DEV, values: FlavorValues(baseUrl: AppConstants.dev_url));

  BlocSupervisor().delegate = SimpleBlocDelegate();
  runApp(AppStateContainer(child: WeatherApp()));
}

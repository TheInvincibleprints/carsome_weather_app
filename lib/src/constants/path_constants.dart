import 'package:carsomeweatherapp/src/config/flavor_config.dart';

final String kBaseAPIURL = FlavorConfig.instance.values.baseUrl;

import 'package:carsomeweatherapp/src/screens/city_screen.dart';
import 'package:carsomeweatherapp/src/screens/main_screen.dart';
import 'package:carsomeweatherapp/src/screens/settings_screen.dart';
import 'package:flutter/material.dart';

class Routes {
  static final mainRoute = <String, WidgetBuilder>{
    '/home': (context) => MainScreen(),
    '/settings': (context) => SettingsScreen()
  };
}
